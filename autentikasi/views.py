from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from . import forms, models
from .models import UserProfile

# Create your views here.
def index(request):
    #return render(request, 'autentikasi/index.html')
    if request.user.is_authenticated:
        name = UserProfile.objects.get(user = request.user)
        data = {
            'name' : name
        }
        return render(request, 'autentikasi/index-authenticated.html', data)
    else:
        return render(request, 'autentikasi/index.html')

def signup(request):
    user_form = forms.CreateUserForm()
    profile_form = forms.UserProfileForm()

    if request.method == 'POST':
        user_form = forms.CreateUserForm(request.POST)
        profile_form = forms.UserProfileForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.save()
            profile = profile_form.save()
            profile.user = user
            profile.save()
            username = user_form.cleaned_data.get('username')
            messages.success(request, 'Account was created for ' + username)
            return redirect('/signin')
    context = { 
        'form':user_form,
        'profile_form':profile_form,
    }
    return render(request, 'autentikasi/signup.html', context)

def signin(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return redirect('/')
        else:
            messages.info(request, 'Username or Password is Incorrect')

    return render(request, 'autentikasi/signin.html')

def logoutUser(request):
    logout(request)
    return render(request, 'autentikasi/signin.html')
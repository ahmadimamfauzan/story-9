from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import UserProfile
from django.contrib.auth.models import User
from .views import index

# Create your tests here.

class StatusTest(TestCase):
    # url_test
    def test_status_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    #test_fungsi_index
    def test_status_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    #test_pakai_template
    def test_article_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'autentikasi/index.html')

class TestAuthApp(TestCase):
    def setUp(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        user.save()

    
    def test_signup_url_exists(self):
        response = self.client.get('/signup/')
        self.assertEquals(response.status_code, 200)

    def test_create_new_user_from_url(self):
        total_user = User.objects.all().count()
        total_profile = UserProfile.objects.all().count()
        passed_data = {
            'username':'thisisusername',
            'password1':'ThisIsAP@ssword123',
            'password2':'ThisIsAP@ssword123',
            'email':'thisisemail@mail.org',
            'full_name':'my full name',
        }
        response = self.client.post('/signup/', data = passed_data)
        self.assertEquals(User.objects.all().count(), total_user+1)
        self.assertEquals(UserProfile.objects.all().count(), total_profile+1)

    def test_signin_url_exists(self):
        response = self.client.get('/signin/')
        self.assertEquals(response.status_code, 200)
    
    def test_success_signin_john(self):
        passed_data = {
            'username':'john',
            'password':'johnpassword'
        }
        response = self.client.post('/signin', data = passed_data)
        content_response = response.content.decode('utf8')
        self.assertEquals(response.status_code, 301)
        self.assertIn(content_response, 'Success')

    def test_failed_signin_john(self):
        passed_data = {
            'username':'john',
            'password':'johnpasswo'
        }
        response = self.client.post('/signin', data =  passed_data)
        content_response = response.content.decode('utf8')
        self.assertEquals(response.status_code, 301)
        self.assertIn(content_response, 'Invalid')